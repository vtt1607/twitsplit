package com.example.application.myapplication

import com.example.application.myapplication.util.extensions.mergeSplitItems
import com.example.application.myapplication.util.extensions.splitMessage
import org.junit.Test
import kotlin.test.*


/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun splitAndMerge1() {
        val message = "I can't believe Tweeter now supports chunking my messages, so I don't have to do it myself."
        val expectText1 = "1/2 I can't believe Tweeter now supports chunking"
        val expectText2 = "2/2 my messages, so I don't have to do it myself."

        assertEquals(message.splitMessage(), listOf(expectText1, expectText2))
        assertEquals(listOf(expectText1, expectText2).mergeSplitItems(), message)
    }

    @Test
    fun splitAndMerge2() {
        val message = "I can't believe Tweeter now supports chunking."
        val expectText = "I can't believe Tweeter now supports chunking."

        assertEquals(message.splitMessage().lastOrNull(), expectText)
        assertEquals(listOf(expectText).mergeSplitItems(), message)
    }

    @Test
    fun splitFail() {
        //the message1 contains a span of non-whitespace characters with 54 characters
        val message1 = "AreyoulookingforatrulyspecialEnglishlearningexperience for your child? Varsity International offer a summer spent learning or improving their English language, taking part in exciting activities integrated with British children. An opportunity for them to sharing their love for sports, arts and adventure with British children in an immersive summer programme for students aged 8-14."
        assertFailsWith(Exception::class) {
            message1.splitMessage()
        }
    }
}