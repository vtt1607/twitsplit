package com.example.application.myapplication.application

import androidx.appcompat.app.AppCompatActivity
import com.example.application.myapplication.R
import com.example.application.myapplication.ui.home.HomeFragment

class Navigator {
    var activity : AppCompatActivity? = null
    fun goToHome(){
        activity?.supportFragmentManager?.beginTransaction()?.replace(R.id.fragmentContainer, HomeFragment.newInstance())?.commit()
    }
}