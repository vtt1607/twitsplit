package com.example.application.myapplication.application

import android.app.Activity
import android.app.Application
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.application.myapplication.di.DaggerViewModelComponent
import com.example.application.myapplication.di.ViewModelComponent
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import javax.inject.Inject

class App : Application(), HasActivityInjector {

    private lateinit var mActivityDispatchingAndroidInjector: DispatchingAndroidInjector<Activity>
    private lateinit var mViewModelFactory: AppViewModelFactory
    private lateinit var mViewModelComponent: ViewModelComponent
    private val mNavigator: Navigator by lazy { Navigator() }

    @Inject
    fun inject(activityDispatchingAndroidInjector: DispatchingAndroidInjector<Activity>,
               viewModelFactory: AppViewModelFactory) {
        mActivityDispatchingAndroidInjector = activityDispatchingAndroidInjector
        mViewModelFactory = viewModelFactory
    }

    fun getViewModelFactory() = mViewModelFactory
    fun getViewModelComponent() = mViewModelComponent
    fun getNavigator() = mNavigator

    override fun onCreate() {
        super.onCreate()
        instance = this
        mViewModelComponent = init(this)
        registerActivityLifecycleCallbacks(object : Application.ActivityLifecycleCallbacks {
            override fun onActivityPaused(p0: Activity?) {
                mNavigator.activity = null
            }
            override fun onActivityResumed(p0: Activity?){
                mNavigator.activity = p0 as? AppCompatActivity
            }
            override fun onActivityStarted(p0: Activity?) = Unit
            override fun onActivityDestroyed(p0: Activity?) = Unit
            override fun onActivitySaveInstanceState(p0: Activity?, p1: Bundle?) = Unit
            override fun onActivityStopped(p0: Activity?) = Unit
            override fun onActivityCreated(p0: Activity?, p1: Bundle?){
                mNavigator.activity = p0 as? AppCompatActivity
            }
        })
    }

    override fun activityInjector(): AndroidInjector<Activity> {
        return mActivityDispatchingAndroidInjector
    }

    companion object {
        lateinit var instance: App
            private set

        fun init(application: App): ViewModelComponent {
            val viewModelComponent = DaggerViewModelComponent
                .builder()
                .application(application)
                .build()
            viewModelComponent.inject(application)
            return viewModelComponent
        }
    }
}