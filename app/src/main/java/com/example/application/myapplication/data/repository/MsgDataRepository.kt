package com.example.application.myapplication.data.repository

import com.example.application.myapplication.data.repository.datafactory.MsgDataStoreFactory
import com.example.application.myapplication.domain.repository.MsgRepository
import io.reactivex.Flowable
import javax.inject.Inject

class MsgDataRepository @Inject constructor(private val factory: MsgDataStoreFactory) : MsgRepository{
    override fun postMsg(msg: String?): Flowable<String> {
        return factory.remote.postMsg(msg)
    }
}