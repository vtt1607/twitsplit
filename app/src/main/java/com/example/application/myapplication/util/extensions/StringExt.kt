package com.example.application.myapplication.util.extensions

fun String.splitMessage(): List<String> {
    return when {
        this.split(" ").any { it.length >= 50 } -> throw Exception()
        this.length <= 50 -> {
            listOf(this)
        }
        else -> {
            val values = mutableListOf<String>()
            var startIndex = 0
            val totalSplit = this.getEstimateSizeSplit()
            val estimateSize = getSizePrefix(totalSplit)
            var endIndex = this.getTextEndIndex(startIndex, estimateSize)
            values.add( this.substring(startIndex, endIndex))
            while (endIndex + 1 < this.length) {
                startIndex = endIndex + 1
                endIndex = this.getTextEndIndex(startIndex, estimateSize)
                values.add(this.substring(startIndex, endIndex))
            }
            val total = values.size
            var index = 0
            values.map {
                index += 1
                "$index/$total $it"
            }
        }
    }
}

fun List<String>.mergeSplitItems(): String {
    return if (this.size > 1) {
        this.sortSplitItems().joinToString(" ") {
            it.substring(it.indexOfFirst { char -> char == ' ' } + 1, it.length)
        }
    } else {
        this[0]
    }
}

private fun List<String>.sortSplitItems(): List<String>{
    return this.sortedBy {
        it.split("/").firstOrNull()?.toInt()
    }
}

private fun getSizePrefix(max: Int): Int {
    return when {
        max < 10 -> 1
        max in 10..99 -> 2
        else -> 3
    }
}

private fun String.getTextEndIndex(index: Int, estimateSize: Int, size: Int = 50): Int {
    return if (index + size - (estimateSize * 2 + 1) >= this.length) {
        this.length
    } else {
        if (this[index + size - (estimateSize * 2 + 1)] == ' ') {
            index + size - (estimateSize * 2 + 1)
        } else {
            index + this.substring(index, index + size - (estimateSize * 2 + 1)).lastIndexOf(' ')
        }
    }
}

private fun String.getEstimateSizeSplit(minPrefix: Int = 1, size: Int = 50): Int {
    val count =
        (this.length / (size - (minPrefix * 2 + 1))) + if (this.length % (size - (minPrefix * 2 + 1)) != 0) 1 else 0
    return when {
        count < 10 -> count
        count in 10..99 -> {
            (this.length / (size - ((minPrefix + 1) * 2 + 1))) + if (this.length % (size - ((minPrefix + 1) * 2 + 1)) != 0) 1 else 0
        }
        else -> {
            (this.length / (size - ((minPrefix + 1) * 2 + 1))) + if (this.length % (size - ((minPrefix + 1) * 2 + 1)) != 0) 1 else 0
        }
    }
}