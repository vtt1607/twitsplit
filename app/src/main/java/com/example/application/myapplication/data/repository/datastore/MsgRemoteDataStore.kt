package com.example.application.myapplication.data.repository.datastore

import com.example.application.myapplication.data.repository.datasource.MsgRemote
import io.reactivex.Flowable
import javax.inject.Inject

class MsgRemoteDataStore @Inject constructor(private val msgRemote: MsgRemote): MsgDataStore {
    override fun postMsg(msg: String?): Flowable<String> {
        return msgRemote.postMsg(msg)
    }
}