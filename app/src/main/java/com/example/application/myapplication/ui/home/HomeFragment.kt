package com.example.application.myapplication.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.request.RequestOptions
import com.example.application.myapplication.R
import com.example.application.myapplication.base.BaseMviFragment
import com.example.application.myapplication.util.extensions.firstClick
import com.example.application.myapplication.util.extensions.hideKeyBoard
import com.example.application.myapplication.util.extensions.loadImage
import com.jakewharton.rxbinding3.widget.textChanges
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.BehaviorSubject
import kotlinx.android.synthetic.main.fragment_home.*

class HomeFragment : BaseMviFragment<HomeIntent, HomeResult, HomeState, HomeViewModel>() {
    private val mPostMsgIntentPublisher = BehaviorSubject.create<HomeIntent.PostMsg>()
    private val mCompositeDisposable = CompositeDisposable()
    private var mMsgAdapter : MsgAdapter?=null

    override val vmClassToken: Class<HomeViewModel>
        get() = HomeViewModel::class.java

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun intents(): Observable<HomeIntent> {
        return mPostMsgIntentPublisher as Observable<HomeIntent>
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initUI()
        initListener()
    }

    private fun initUI() {
        userIcon.loadImage(R.drawable.ic_pig,RequestOptions().circleCrop())
        sendMsg.loadImage(R.drawable.ic_default, isRounded = true)
        if(mMsgAdapter == null){
            mMsgAdapter = MsgAdapter()
        }
        listMsg.layoutManager = LinearLayoutManager(context)
        listMsg.adapter = mMsgAdapter
    }

    private fun initListener() {
        mCompositeDisposable.add(inputText.textChanges().subscribe {
            sendMsg.isEnabled = it.any()
        })
        main.firstClick({
            context?.hideKeyBoard()
        })
        listMsg.setOnTouchListener { _, motionEvent ->
            if(motionEvent.action == MotionEvent.ACTION_UP){
                context?.hideKeyBoard()
                return@setOnTouchListener true
            }
            return@setOnTouchListener false
        }
        sendMsg.firstClick({
            mPostMsgIntentPublisher.onNext(HomeIntent.PostMsg(inputText.text.trim().toString()))
        })
    }

    override fun render(state: HomeState) {
        when (state.action) {
            HomeState.HomeUiAction.IDLE ->{

            }
            HomeState.HomeUiAction.LOADING -> {

            }
            HomeState.HomeUiAction.POST_MSG -> {
                inputText.setText("")
                context?.hideKeyBoard()
                mMsgAdapter?.submitList(state.listMsg.reversed())
            }
            HomeState.HomeUiAction.ERROR -> {
                showToastError(state.error)
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        mCompositeDisposable.dispose()
    }


    companion object {
        fun newInstance() = HomeFragment()
    }
}
