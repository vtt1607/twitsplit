package com.example.application.myapplication.domain.interactor

import com.example.application.myapplication.domain.executor.PostExecutionThread
import com.example.application.myapplication.domain.executor.ThreadExecutor
import io.reactivex.Flowable
import io.reactivex.schedulers.Schedulers

abstract class FlowableUseCase<R, in Params> constructor(
    private val threadExecutor: ThreadExecutor,
    private val postExecutionThread: PostExecutionThread
) {
    protected abstract fun buildUseCaseObservable(params: Params?): Flowable<R>
    open fun execute(params: Params? = null): Flowable<R> {
        return this.buildUseCaseObservable(params)
            .subscribeOn(Schedulers.from(threadExecutor))
            .observeOn(postExecutionThread.scheduler)
    }
}