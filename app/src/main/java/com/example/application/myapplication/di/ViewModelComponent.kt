package com.example.application.myapplication.di

import android.app.Application
import com.example.application.myapplication.application.App
import dagger.BindsInstance
import dagger.Component
import com.example.application.myapplication.di.module.ApplicationModule
import com.example.application.myapplication.di.module.DataModule
import com.example.application.myapplication.di.module.DomainModule
import com.example.application.myapplication.di.module.MainModule
import com.example.application.myapplication.ui.home.HomeViewModel
import javax.inject.Singleton

@Singleton
@Component(modules = [(ApplicationModule::class),(DataModule::class),(DomainModule::class),(MainModule::class)])
interface ViewModelComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder
        fun build(): ViewModelComponent
    }
    fun inject(app: App)
    fun inject(vm: HomeViewModel)
}