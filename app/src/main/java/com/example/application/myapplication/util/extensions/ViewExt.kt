package com.example.application.myapplication.util.extensions

import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.example.application.myapplication.application.App
import com.jakewharton.rxbinding3.view.clicks
import io.reactivex.android.schedulers.AndroidSchedulers
import java.util.concurrent.TimeUnit

fun View.firstClick(cb: () -> Unit, duration: Long = 1000) {
    this.clicks()
        .throttleFirst(duration, TimeUnit.MILLISECONDS)
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe {
            cb()
        }
}

fun ImageView.loadImage(
    url: Any?,
    requestOptions: RequestOptions? = null,
    isRounded: Boolean = false
) {
    val glide = Glide.with(App.instance.applicationContext)
    glide.clear(this)
    if (url != null && (url is Int || url is String)) {
        try {
            val requestBuilder = glide.load(url)
            requestOptions?.let {
                requestBuilder.apply(it)
            }
            requestBuilder.apply(RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL))
            if (isRounded) {
                requestBuilder.apply(RequestOptions().transforms(CenterCrop(), RoundedCorners(32)))
            }
            requestBuilder.into(this)
        } catch (e: Exception) {
        }
    }
}