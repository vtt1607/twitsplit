package com.example.application.myapplication.ui.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.AsyncDifferConfig
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.application.myapplication.R
import com.example.application.myapplication.base.IModel
import com.example.application.myapplication.util.NoteDiffCallBack
import kotlinx.android.synthetic.main.item_msg.view.*
import java.util.concurrent.Executors

class MsgAdapter : ListAdapter<IModel, MsgAdapter.ItemViewHolder>(
        AsyncDifferConfig.Builder<IModel>(NoteDiffCallBack())
            .setBackgroundThreadExecutor(Executors.newSingleThreadExecutor())
            .build()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_msg, parent, false)
        return ItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val item = getItem(position) as? MsgItemModel
        holder.setIsRecyclable(false)
        holder.msg.text = item?.msg
    }

    override fun submitList(list: List<IModel>?) {
        super.submitList(ArrayList<IModel>(list ?: listOf()))
    }

    inner class ItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var msg: TextView = view.msg
    }
}