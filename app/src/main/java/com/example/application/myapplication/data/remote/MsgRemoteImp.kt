package com.example.application.myapplication.data.remote

import com.example.application.myapplication.data.remote.service.MsgService
import com.example.application.myapplication.data.repository.datasource.MsgRemote
import io.reactivex.Flowable
import javax.inject.Inject

class MsgRemoteImp @Inject constructor(private val msgService: MsgService) : MsgRemote {
    override fun postMsg(msg: String?): Flowable<String> {
        //send msg to server
        return Flowable.just(msg)
    }
}