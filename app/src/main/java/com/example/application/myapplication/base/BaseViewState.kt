package com.example.application.myapplication.base

typealias ErrorCallbackHandler = ()->Unit

interface BaseViewState