package com.example.application.myapplication.di.module

import com.example.application.myapplication.application.MainActivity
import com.example.application.myapplication.application.UiThread
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import com.example.application.myapplication.domain.executor.PostExecutionThread

@Module
abstract class MainModule {
    @Binds
    abstract fun bindPostExecutionThread(uiThread: UiThread): PostExecutionThread

    @ContributesAndroidInjector
    abstract fun contributeMainActivity(): MainActivity
}