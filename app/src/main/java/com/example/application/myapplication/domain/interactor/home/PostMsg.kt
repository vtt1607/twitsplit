package com.example.application.myapplication.domain.interactor.home

import com.example.application.myapplication.domain.executor.PostExecutionThread
import com.example.application.myapplication.domain.executor.ThreadExecutor
import com.example.application.myapplication.domain.interactor.FlowableUseCase
import com.example.application.myapplication.domain.repository.MsgRepository
import io.reactivex.Flowable
import javax.inject.Inject

class PostMsg @Inject constructor(
    private val msgRepository: MsgRepository,
    threadExecutor: ThreadExecutor,
    postExecutionThread: PostExecutionThread
) : FlowableUseCase<String, String?>(threadExecutor, postExecutionThread) {
    override fun buildUseCaseObservable(params: String?): Flowable<String> {
        return msgRepository.postMsg(params)
    }
}