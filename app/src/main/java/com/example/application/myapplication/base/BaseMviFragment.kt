package com.example.application.myapplication.base

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.example.application.myapplication.application.App
import com.example.application.myapplication.util.extensions.subscribe

abstract class BaseMviFragment<I : BaseIntent, R : BaseResult, S : BaseViewState, VM : BaseViewModel<I, R, S>> :Fragment(), BaseView<I, S> {
    protected abstract val vmClassToken: Class<VM>
    private val viewModel: VM by lazy {
        ViewModelProviders.of(this, App.instance.getViewModelFactory())[vmClassToken]
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.processIntents(intents())
        viewModel.state().subscribe(this) {
            render(it)
        }
    }

    fun showToastError(error: String?){
        Toast.makeText(context, error, Toast.LENGTH_SHORT).show()
    }
}