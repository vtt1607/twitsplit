package com.example.application.myapplication.di.module

import dagger.Binds
import dagger.Module
import com.example.application.myapplication.application.SharedPreferencesHelper
import com.example.application.myapplication.application.SharedPreferencesHelperImp
import com.example.application.myapplication.data.cache.MsgCacheImp
import com.example.application.myapplication.data.remote.MsgRemoteImp
import com.example.application.myapplication.data.remote.service.MsgService
import com.example.application.myapplication.data.remote.util.ServiceFactory
import com.example.application.myapplication.data.repository.datasource.MsgCache
import com.example.application.myapplication.data.repository.datasource.MsgRemote
import dagger.Provides

@Module
abstract class DataModule {
    @Module
    companion object {
        @Provides
        @JvmStatic
        fun provideMsgService(): MsgService {
            return ServiceFactory.makeService(MsgService::class.java)
        }
    }

    @Binds
    abstract fun bindSharedPreferencesHelper(sharedPreferencesHelperImp: SharedPreferencesHelperImp): SharedPreferencesHelper

    @Binds
    abstract fun bindMsgCache(msgCacheImp: MsgCacheImp): MsgCache

    @Binds
    abstract fun bindMsgRemote(msgRemoteImp: MsgRemoteImp): MsgRemote
}