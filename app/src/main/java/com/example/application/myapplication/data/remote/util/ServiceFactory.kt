package com.example.application.myapplication.data.remote.util

import com.example.application.myapplication.BuildConfig
import com.squareup.moshi.FromJson
import com.squareup.moshi.Moshi
import com.squareup.moshi.ToJson
import okhttp3.Interceptor
import okhttp3.MediaType
import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

object ServiceFactory {
    fun <T> makeService(type: Class<T>): T {
        val retrofit = makeRetrofit()
        return retrofit.create(type)
    }

    private fun makeRetrofit(): Retrofit {
        val okHttpClient = makeOkHttpClient()
        return Retrofit.Builder().baseUrl(APIConstant.BASE_URL)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(createMoshiConverter())
            .client(okHttpClient)
            .build()
    }

    private fun createMoshiConverter(): MoshiConverterFactory = MoshiConverterFactory.create(makeMoshi())

    private fun makeMoshi(): Moshi {
        val customDateAdapter: Any = object : Any() {
            val dateFormat: DateFormat

            init {
                dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm")
                dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"))
            }

            @ToJson
            @Synchronized
            fun dateToJson(d: Date): String {
                return dateFormat.format(d)
            }

            @FromJson
            @Synchronized
            @Throws(ParseException::class)
            fun dateToJson(s: String): Date {
                return dateFormat.parse(s)
            }
        }
        return Moshi.Builder()
            .add(customDateAdapter)
            .build()
    }

    private fun makeOkHttpClient(): OkHttpClient {
        val httpLoggingInterceptor =
            makeLoggingInterceptor()

        return OkHttpClient.Builder()
            .addInterceptor(httpLoggingInterceptor)
            .addInterceptor(GeneralInterceptor())
            .addInterceptor {
                val request = it.request().newBuilder()
                it.proceed(request.build())
            }
            .connectTimeout(APIConstant.TIME_OUT, TimeUnit.SECONDS)
            .readTimeout(APIConstant.TIME_OUT, TimeUnit.SECONDS)
            .build()
    }

    private fun makeLoggingInterceptor(): HttpLoggingInterceptor {
        val isDebug = BuildConfig.DEBUG
        val logging = HttpLoggingInterceptor()
        logging.level = if (isDebug)
            HttpLoggingInterceptor.Level.BODY
        else
            HttpLoggingInterceptor.Level.NONE
        return logging
    }

    private class GeneralInterceptor : Interceptor {
        override fun intercept(chain: Interceptor.Chain?): okhttp3.Response? {
            val originalRequest = chain?.request()
            val requestBuilder = originalRequest?.newBuilder()
            var response = chain?.proceed(requestBuilder?.build())
            if (response?.code() == 204) {
                response = response.newBuilder().code(200).body(ResponseBody.create(MediaType.parse("application/json"), "{}")).build()
            }
            return response
        }
    }
}