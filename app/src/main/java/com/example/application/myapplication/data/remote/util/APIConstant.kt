package com.example.application.myapplication.data.remote.util

object APIConstant {
    const val BASE_URL = "https://console.cloud.google.com/apis/library/"
    const val TIME_OUT = 120L
}