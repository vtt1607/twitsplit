package com.example.application.myapplication.application

import android.content.Context
import android.content.SharedPreferences
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SharedPreferencesHelperImp @Inject constructor(val context: Context) : SharedPreferencesHelper {
    private var prefs: SharedPreferences

    init {
        prefs = context.getSharedPreferences(PREFS_FILENAME, Context.MODE_PRIVATE)
    }

    companion object {
        private const val PREFS_FILENAME = "app.test"
    }
}