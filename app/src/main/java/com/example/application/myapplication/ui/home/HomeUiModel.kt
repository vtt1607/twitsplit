package com.example.application.myapplication.ui.home

import com.example.application.myapplication.base.BaseIntent
import com.example.application.myapplication.base.BaseResult
import com.example.application.myapplication.base.BaseViewState
import com.example.application.myapplication.base.IModel
import com.example.application.myapplication.util.enums.TaskStatus

sealed class HomeIntent : BaseIntent {
    data class PostMsg(val msg: String) : HomeIntent()
}

sealed class HomeResult : BaseResult {
    class PostMsgTask(val status: TaskStatus,val msg: String? = null, val error: String? = null) : HomeResult() {
        companion object {
            internal fun success(msg: String): PostMsgTask {
                return PostMsgTask(TaskStatus.SUCCESS, msg)
            }

            internal fun failure(error: String?): PostMsgTask {
                return PostMsgTask(TaskStatus.FAILURE,error = error)
            }

            internal fun inFlight(): PostMsgTask {
                return PostMsgTask(TaskStatus.IN_FLIGHT)
            }
        }
    }
}

data class MsgItemModel(override var id: Int = 0, val msg: String?) : IModel

data class HomeState(
    val listMsg: List<MsgItemModel> = listOf(),
    val currentPage: Int = 0,
    val totalPage: Int = 0,
    val error: String? = null,
    val action: HomeUiAction = HomeUiAction.IDLE
) : BaseViewState {
    enum class HomeUiAction {
        IDLE,
        LOADING,
        POST_MSG,
        ERROR
    }
}