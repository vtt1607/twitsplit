package com.example.application.myapplication.util.extensions

import android.app.Activity
import android.content.Context
import android.content.res.Configuration
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.view.ContextThemeWrapper
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.example.application.myapplication.application.OnBackPressed

fun Context.hideKeyBoard() {
    val activity = (this as? Activity ?: (this as? ContextThemeWrapper)?.baseContext as? Activity)
    activity?.let {
        val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
        imm?.hideSoftInputFromWindow(activity.currentFocus?.windowToken, 0)
        activity.currentFocus?.clearFocus()
    }
}

fun AppCompatActivity.recursivelyDispatchOnBackPressed() : Boolean{
    val fm = supportFragmentManager
    if (fm.backStackEntryCount == 0)
        return false
    return dispatchOnBackPressed(fm)
}

private fun dispatchOnBackPressed(fm: FragmentManager) : Boolean{
    val reverseOrder = fm.fragments.filter { it is OnBackPressed }.reversed()
    for (f in reverseOrder) {
        val handledByChildFragments = dispatchOnBackPressed(f.childFragmentManager)
        if (handledByChildFragments) {
            return true
        }

        val backPressable = f as OnBackPressed
        if (backPressable.onBackPressed()) {
            return true
        }
    }
    return false
}
