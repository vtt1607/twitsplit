package com.example.application.myapplication.application

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.application.myapplication.R
import com.example.application.myapplication.util.extensions.recursivelyDispatchOnBackPressed

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        App.instance.getNavigator().goToHome()
    }

    override fun onBackPressed() {
        val handled = recursivelyDispatchOnBackPressed()
        if (!handled) {
            super.onBackPressed()
        }
    }
}
