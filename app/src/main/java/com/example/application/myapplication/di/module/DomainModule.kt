package com.example.application.myapplication.di.module

import com.example.application.myapplication.data.repository.MsgDataRepository
import dagger.Binds
import dagger.Module
import com.example.application.myapplication.domain.executor.JobExecutor
import com.example.application.myapplication.domain.executor.ThreadExecutor
import com.example.application.myapplication.domain.repository.MsgRepository

@Module
abstract class DomainModule {

    @Binds
    abstract fun bindThreadExecutor(jobExecutor: JobExecutor): ThreadExecutor

    @Binds
    abstract fun bindMsgRepository(msgDataRepository: MsgDataRepository): MsgRepository
}