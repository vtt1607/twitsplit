package com.example.application.myapplication.domain.repository

import io.reactivex.Flowable

interface MsgRepository{
    fun postMsg(msg:String?) : Flowable<String>
}