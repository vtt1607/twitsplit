package com.example.application.myapplication.util.extensions

import retrofit2.HttpException
import java.net.SocketException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

fun Throwable.parse(): String? {
    return when (this) {
        is HttpException -> {
            when {
                this.code() < 407 -> this.response().errorBody()?.string()
                this.code() in 408..499 -> this.response().errorBody()?.string()
                else -> "A temporary server error has occurred. Please try again."
            }
        }
        is UnknownHostException, is SocketException, is SocketTimeoutException ->{
            "A temporary server error has occurred. Please try again."
        }
        else -> {
            "Unknown error. Please try again."
        }
    }
}