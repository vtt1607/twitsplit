package com.example.application.myapplication.data.repository.datafactory

abstract class BaseDataStoreFactory<D, C : D,R: D> {
    abstract val cache: C
    abstract val remote: R
    open fun retrieveDataStore(isCached: Boolean = false): D {
        if (isCached) {
            return retrieveCacheDataStore()
        }
        return retrieveRemoteDataStore()
    }
    open fun retrieveCacheDataStore(): D {
        return cache
    }
    open fun retrieveRemoteDataStore(): D {
        return remote
    }
}