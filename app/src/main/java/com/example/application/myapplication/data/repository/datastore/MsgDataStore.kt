package com.example.application.myapplication.data.repository.datastore

import io.reactivex.Flowable

interface MsgDataStore {
    fun postMsg(msg: String?): Flowable<String>
}
