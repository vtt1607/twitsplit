package com.example.application.myapplication.application

interface OnBackPressed {
    fun onBackPressed(): Boolean
}