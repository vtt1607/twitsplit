package com.example.application.myapplication.util

import androidx.recyclerview.widget.DiffUtil
import com.example.application.myapplication.base.IModel

class NoteDiffCallBack : DiffUtil.ItemCallback<IModel>() {
    override fun areItemsTheSame(oldItem: IModel, newItem: IModel): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: IModel, newItem: IModel): Boolean {
        return oldItem == newItem
    }
}