package com.example.application.myapplication.ui.home

import com.example.application.myapplication.base.BaseViewModel
import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import com.example.application.myapplication.util.enums.TaskStatus
import com.example.application.myapplication.domain.interactor.home.PostMsg
import com.example.application.myapplication.util.extensions.mergeSplitItems
import com.example.application.myapplication.util.extensions.parse
import com.example.application.myapplication.util.extensions.splitMessage
import io.reactivex.Flowable
import javax.inject.Inject

class HomeViewModel : BaseViewModel<HomeIntent, HomeResult, HomeState>() {
    private lateinit var postMsg: PostMsg

    @Inject
    fun injectDi(postMsg: PostMsg) {
        this.postMsg = postMsg
    }

    override val initialState: HomeState
        get() = HomeState()

    override val actionForIntent: ObservableTransformer<HomeIntent, HomeResult>
        get() = ObservableTransformer {
            it.publish { intent ->
                intent.ofType(HomeIntent.PostMsg::class.java).compose(signInProcessor)
                    .mergeWith(intent.filter { v ->
                        v !is HomeIntent.PostMsg
                    }.flatMap {
                        Observable.error<HomeResult>(IllegalArgumentException("Unknown intent type"))
                    })
            }
        }

    override fun processReducer(state: HomeState, result: HomeResult): HomeState {
        return when (result) {
            is HomeResult.PostMsgTask -> {
                when (result.status) {
                    TaskStatus.SUCCESS -> {
                        state.copy(
                            listMsg = state.listMsg.plus(MsgItemModel(id=((state.listMsg.lastOrNull()?.id?:0) +1),msg = result.msg)),
                            action = HomeState.HomeUiAction.POST_MSG
                        )
                    }
                    TaskStatus.FAILURE -> {
                        state.copy(
                            error = result.error,
                            action = HomeState.HomeUiAction.ERROR
                        )
                    }
                    TaskStatus.IN_FLIGHT -> {
                        state.copy(
                            action = HomeState.HomeUiAction.LOADING
                        )
                    }
                }
            }
        }
    }

    private val signInProcessor: ObservableTransformer<HomeIntent.PostMsg, HomeResult> = ObservableTransformer {
        it.switchMap {intent->
             try {
                 Flowable.zip(intent.msg.splitMessage().map {string->
                     postMsg.execute(string)
                 }) {out->
                     if(out.all {item-> item is String }){
                         (out.toList() as? List<String>)?.mergeSplitItems()?.trim()
                     }else{
                         ""
                     }
                 }.map {msg->
                     HomeResult.PostMsgTask.success(msg)
                 }.onErrorReturn { error ->
                     HomeResult.PostMsgTask.failure(error.parse())
                 }.toObservable()
                     .startWith(HomeResult.PostMsgTask.inFlight())
            }catch (e: Exception){
                 Observable.just(HomeResult.PostMsgTask.failure("Error : word longer than 50 characters"))
            }
        }
    }
}
