package com.example.application.myapplication.data.repository.datasource

import io.reactivex.Flowable

interface MsgRemote {
    fun postMsg(msg: String?): Flowable<String>
}