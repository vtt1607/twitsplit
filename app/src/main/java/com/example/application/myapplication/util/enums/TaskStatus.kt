package com.example.application.myapplication.util.enums

enum class TaskStatus {
    SUCCESS, FAILURE, IN_FLIGHT
}