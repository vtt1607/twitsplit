package com.example.application.myapplication.application

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.application.myapplication.ui.home.HomeViewModel
import javax.inject.Inject

class AppViewModelFactory @Inject constructor() : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T = when (modelClass) {
        HomeViewModel::class.java -> {
            HomeViewModel().apply {
                App.instance.getViewModelComponent().inject(this)
            }
        }
       else -> throw IllegalArgumentException("No ViewModel registered for $modelClass")
    } as T
}