package com.example.application.myapplication.base

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import io.reactivex.disposables.Disposable
import io.reactivex.functions.BiFunction
import io.reactivex.subjects.BehaviorSubject

abstract class BaseViewModel<I : BaseIntent, R : BaseResult, S : BaseViewState> : ViewModel() {
    abstract val initialState: S
    private lateinit var disposable: Disposable
    private val intentsSubject: BehaviorSubject<I> = BehaviorSubject.create()
    private val reducer: BiFunction<S, R, S> = BiFunction { previous, result ->
        processReducer(previous, result)
    }

    open val stateSubject: MutableLiveData<S> = MutableLiveData()

    abstract val actionForIntent: ObservableTransformer<I, R>

    open fun processIntents(intents: Observable<I>) {
        disposable = intentsSubject
            .compose(actionForIntent)
            .scan<S>(initialState, reducer)
            .replay(1)
            .autoConnect(0).subscribe {
                stateSubject.value = it
            }
        intents.subscribe(intentsSubject)
    }

    fun state(): MutableLiveData<S> {
        return stateSubject
    }

    val retry = {
        intentsSubject.value?.let { intentsSubject.onNext(it) }
    }

    fun publishIntent(intent: I) {
        intentsSubject.onNext(intent)
    }

    abstract fun processReducer(state: S, result: R): S
    override fun onCleared() {
        super.onCleared()
        disposable.dispose()
    }
}