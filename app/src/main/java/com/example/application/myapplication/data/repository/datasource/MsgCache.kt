package com.example.application.myapplication.data.repository.datasource

import io.reactivex.Flowable

interface MsgCache{
    fun saveMsg(msg: String?): Flowable<Boolean>
}