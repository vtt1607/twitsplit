package com.example.application.myapplication.data.repository.datafactory

import com.example.application.myapplication.data.repository.datastore.MsgCacheDataStore
import com.example.application.myapplication.data.repository.datastore.MsgDataStore
import com.example.application.myapplication.data.repository.datastore.MsgRemoteDataStore
import javax.inject.Inject

open class MsgDataStoreFactory @Inject constructor(
    override val cache: MsgCacheDataStore,
    override val remote: MsgRemoteDataStore
): BaseDataStoreFactory<MsgDataStore, MsgCacheDataStore, MsgRemoteDataStore>()